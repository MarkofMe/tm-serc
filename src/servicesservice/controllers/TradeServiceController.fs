﻿namespace Services.Controllers

open System
open System.Web.Http
open Services.TSCode
open System.Net.Http

[<RoutePrefixAttribute("TS")>]
type tradeServicesController() =
    inherit ApiController()

    [<Route("All")>]
    member this.All (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("ByDealership")>]
    member this.ByDealership (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAllByDealership(jsonContent)

    [<Route("ByID")>]
    member this.ById (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetById(jsonContent)

    [<Route("ByInvoice")>]
    member this.ByInvoice (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetByInvoice(jsonContent)

    [<Route("InsertTrade")>]
    member this.InsertServiceBooking (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        insertTrade(jsonContent)
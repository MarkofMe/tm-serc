﻿namespace Services.Controllers

open System
open System.Web.Http
open Services.MSCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

[<RoutePrefixAttribute("MS")>]
type mechanicSerivcesController() =
    inherit ApiController()

    [<Route("All")>]
    member this.All (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("AllCompleted")>]
    member this.AllCompleted (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAllCompleted(jsonContent)

    [<Route("AllByDealership")>]
    member this.AllByDealership (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAllbyDealership(jsonContent)

    [<Route("AllByDealershipCompleted")>]
    member this.AllByDealershipCompleted (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAllbyDealershipCompleted(jsonContent)

    [<Route("ByID")>]
    member this.ById (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetById(jsonContent)

    [<Route("CompletedServiceLogsByServiceBookingID")>]
    member this.CompletedServiceLogsByServiceBookingID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetCompletedServiceLogsByServiceBookingID(jsonContent)

    [<Route("ServiceLogsByServiceBookingID")>]
    member this.ServiceLogsByServiceBookingID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetServiceLogsByServiceBookingID(jsonContent)

    [<Route("ByServiceLogID")>]
    member this.ByServiceLogID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetByServiceLogID(jsonContent)
    
    [<Route("InsertServiceLog")>]
    member this.InsertServiceLog (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        insertServiceLog(jsonContent)

    [<Route("CompleteServiceLog")>]
    member this.CompleteServiceLog (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        completeServiceLog(jsonContent)

    [<Route("InsertServiceCompleted")>]
    member this.InsertServiceCompleted (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        insertServiceCompleted(jsonContent)

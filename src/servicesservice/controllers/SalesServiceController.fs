﻿namespace Services.Controllers

open System
open System.Web.Http
open Services.SSCode
open System.Net.Http

[<RoutePrefixAttribute("SS")>]
type salesServicesController() =
    inherit ApiController()

    [<Route("All")>]
    member this.All (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("ByDealership")>]
    member this.ByDealership (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAllByDealership(jsonContent)

    [<Route("ByID")>]
    member this.ById (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetById(jsonContent)

    [<Route("ByInvoice")>]
    member this.ByInvoice (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetByInvoice(jsonContent)

    [<Route("InsertSale")>]
    member this.InsertServiceBooking (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        insertSale(jsonContent)
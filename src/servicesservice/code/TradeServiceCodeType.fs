namespace Services

open System
open System.IO
open System.Net
open FSharp.Core
open DataLayer.DLCode
open System.Drawing
open System.Text

module TSTypes =
    
    type TradeAuth = {
                        UserName : string
                        Password : string
                        }

    type TradeAuthDealership = {
                        UserName : string
                        Password : string
                        DealershipName : string
                        }

    type TradeAuthplusID = {
                            UserName : string
                            Password : string
                            TradeID : Nullable<int>
                            }

    type TradeAuthplusInvoice = {
                            UserName : string
                            Password : string
                            InvoiceID : Nullable<int>
                            }

    type position = {
                    Position : string
                    }
    
    type Trade = {
                    TradeID : Nullable<int>
                    SoldCarDescription : string
                    TradeCarDescription : string
                    StaffName : string
                    DealershipName : string
                    CustomerName : string
                    Date : Nullable<DateTime>
                    }

    type TradeView = {
                    TradeID : Nullable<int>
                    Amount : Nullable<float>
                    SoldCarID : Nullable<int>
                    SoldCarDescription : string
                    TradeCarID : Nullable<int>
                    TradeCarDescription : string
                    StaffID : Nullable<int>
                    StaffName : string
                    DealershipName : string
                    CustomerID : Nullable<int>
                    CustomerName : string
                    Date : Nullable<DateTime>
                    InvoiceID : Nullable<int>
                    }
    
    type InsertTrade = {
                            UserName : string
                            Password : string
                            SoldCarID : Nullable<int>
                            TradeCarID : Nullable<int>
                            CustomerID : Nullable<int>
                            StaffID : Nullable<int>
                            DealershipID : string
                            Amount : Nullable<float>
                            Notes : string
                            }
                            
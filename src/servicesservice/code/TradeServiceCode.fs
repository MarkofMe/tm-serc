namespace Services

open System
open System.Net
open FSharp.Core
open DataLayer.DLCode
open Newtonsoft.Json
open TSTypes
open emailSender

module TSCode =
    
    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()

    let emptyTrade = {
                        TradeID = nullable 0
                        SoldCarDescription = ""
                        TradeCarDescription = ""
                        StaffName = ""
                        DealershipName = ""
                        CustomerName = ""
                        Date = nullable(new DateTime())
                        }

    let emptyTradeView = {
                        TradeID = nullable 0
                        Amount = nullable 0.0
                        SoldCarID = nullable 0
                        SoldCarDescription = ""
                        TradeCarID = nullable 0
                        TradeCarDescription = ""
                        StaffID = nullable 0
                        StaffName = ""
                        DealershipName = ""
                        CustomerID = nullable 0
                        CustomerName = ""
                        Date = nullable(new DateTime())
                        InvoiceID = nullable 0
                        }

    let AuthCheck (creds : TradeAuth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]
    
    let GetAll(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<TradeAuth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->
                        query {
                            for row in schema.TradeGetAll() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    TradeID = row.TradeID
                                                    SoldCarDescription = row.SoldCar
                                                    TradeCarDescription = row.TradeCar
                                                    StaffName = row.StaffName
                                                    DealershipName = row.Name
                                                    CustomerName = row.CustomerName
                                                    Date = row.Date
                                                    }) |> Seq.toArray
            |_-> [|emptyTrade|]
        with ex ->
            [|emptyTrade|]
    
    let GetAllByDealership(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<TradeAuthDealership>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            DealershipName = ""
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->
                    query {
                        for row in schema.TradeGetByDealership(id.DealershipName) do
                        select row
                    } |> Seq.map (fun row -> {
                                                    TradeID = row.TradeID
                                                    Amount = row.Amount
                                                    SoldCarID = row.SoldCarID
                                                    SoldCarDescription = row.SoldCar
                                                    TradeCarID = row.TradeCarID
                                                    TradeCarDescription = row.TradeCar
                                                    StaffID = row.StaffId
                                                    StaffName = row.StaffName
                                                    DealershipName = row.Name
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    Date = row.Date
                                                    InvoiceID = row.InvoiceID
                                                    }) |> Seq.toArray
            |_-> [|emptyTradeView|]
        with ex ->
            [|emptyTradeView|]

    let GetById(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<TradeAuthplusID>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            TradeID = nullable 0
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                    query {
                        for row in schema.TradeGetByID(id.TradeID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                    TradeID = row.TradeID
                                                    Amount = row.Amount
                                                    SoldCarID = row.SoldCarID
                                                    SoldCarDescription = row.SoldCar
                                                    TradeCarID = row.TradeCarID
                                                    TradeCarDescription = row.TradeCar
                                                    StaffID = row.StaffId
                                                    StaffName = row.StaffName
                                                    DealershipName = row.Name
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    Date = row.Date
                                                    InvoiceID = row.InvoiceID
                                                    }) |> Seq.toArray
            |_-> [|emptyTradeView|]
        with ex ->
            [|emptyTradeView|]

    let GetByInvoice(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<TradeAuthplusInvoice>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            InvoiceID = nullable 0
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                    query {
                        for row in schema.TradeGetByInvoiceID(id.InvoiceID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                    TradeID = row.TradeID
                                                    Amount = row.Amount
                                                    SoldCarID = row.SoldCarID
                                                    SoldCarDescription = row.SoldCar
                                                    TradeCarID = row.TradeCarID
                                                    TradeCarDescription = row.TradeCar
                                                    StaffID = row.StaffId
                                                    StaffName = row.StaffName
                                                    DealershipName = row.Name
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    Date = row.Date
                                                    InvoiceID = row.InvoiceID
                                                    }) |> Seq.toArray
            |_-> [|emptyTradeView|]
        with ex ->
            [|emptyTradeView|]
    
    let insertTrade(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<InsertTrade>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            SoldCarID = nullable 0
                            TradeCarID = nullable 0
                            CustomerID = nullable 0
                            StaffID = nullable 0
                            DealershipID = ""
                            Amount = nullable 0.0
                            Notes = ""
                            }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->
                    schema.TradeInsert(id.SoldCarID, id.TradeCarID, id.CustomerID, id.StaffID, id.DealershipID, id.Amount, id.Notes) |> ignore
                    Sorter id.UserName id.Password id.CustomerID.Value "TRADE" data
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Forbidden

            
        with ex ->
            HttpStatusCode.InternalServerError

namespace Services

open System
open System.IO
open System.Net
open FSharp.Core
open DataLayer.DLCode
open System.Drawing
open System.Text
open Newtonsoft.Json
open SSTypes
open emailSender

module SSCode =
    
    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()

    let emptySales = {
                        SaleID = nullable 0
                        CarDescription = ""
                        StaffName = ""
                        DealershipName = ""
                        CustomerName = ""
                        Date = nullable(new DateTime())
                        }

    let emptySalesView = {
                        SaleID = nullable 0
                        Amount = nullable 0.0
                        CarID = nullable 0
                        CarDescription = ""
                        StaffID = nullable 0
                        StaffName = ""
                        DealershipName = ""
                        CustomerID = nullable 0
                        CustomerName = ""
                        Date = nullable(new DateTime())
                        InvoiceID = nullable 0
                        }

    let AuthCheck (creds : SalesAuth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]
    
    let GetAll(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<SalesAuth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->
                        query {
                            for row in schema.SalesGetAll() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    SaleID = row.SaleID
                                                    CarDescription = row.Car
                                                    StaffName = row.StaffName
                                                    DealershipName = row.Name
                                                    CustomerName = row.CustomerName
                                                    Date = row.Date
                                                    }) |> Seq.toArray
            |_-> [|emptySales|]
        with ex ->
            [|emptySales|]

    let GetAllByDealership(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<SalesAuthDealership>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                                DealershipName = ""
                            }

            let authcheck = AuthCheck ({
                                            UserName = creds.UserName
                                            Password = creds.Password
                                        })

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->
                        query {
                            for row in schema.SalesGetByDealership(creds.DealershipName) do
                            select row
                        } |> Seq.map (fun row -> {
                                                    SaleID = row.SaleID
                                                    CarDescription = row.Car
                                                    StaffName = row.StaffName
                                                    DealershipName = row.Name
                                                    CustomerName = row.CustomerName
                                                    Date = row.Date
                                                    }) |> Seq.toArray
            |_-> [|emptySales|]
        with ex ->
            [|emptySales|]
            
    let GetById(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<SalesAuthplusID>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            SalesID = nullable 0
                        }

            let creds = 
                        {
                            UserName = id.UserName
                            Password = id.Password
                        }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                    query {
                        for row in schema.SalesGetByID(id.SalesID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                SaleID = row.SaleID
                                                Amount = row.Amount
                                                CarID = row.CarId
                                                CarDescription = row.Car
                                                StaffID = row.StaffId
                                                StaffName = row.StaffName
                                                DealershipName = row.Name
                                                CustomerID = row.CustomerID
                                                CustomerName = row.CustomerName
                                                Date = row.Date
                                                InvoiceID = row.InvoiceID
                                                }) |> Seq.toArray
            |_-> [|emptySalesView|]
        with ex ->
            [|emptySalesView|]

    let GetByInvoice(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<SalesAuthplusInvoice>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            InvoiceID = nullable 0
                        }

            let creds = 
                        {
                            UserName = id.UserName
                            Password = id.Password
                        }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") with
            | true ->
                    query {
                        for row in schema.SalesGetByInvoiceID(id.InvoiceID) do
                        select row
                    } |> Seq.map (fun row -> {
                                                SaleID = row.SaleID
                                                Amount = row.Amount
                                                CarID = row.CarId
                                                CarDescription = row.Car
                                                StaffID = row.StaffId
                                                StaffName = row.StaffName
                                                DealershipName = row.Name
                                                CustomerID = row.CustomerID
                                                CustomerName = row.CustomerName
                                                Date = row.Date
                                                InvoiceID = row.InvoiceID
                                                }) |> Seq.toArray
            |_-> [|emptySalesView|]
        with ex ->
            [|emptySalesView|]
    
    let insertSale(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<InsertSales>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            CarID = nullable 0
                            CustomerID = nullable 0
                            StaffID = nullable 0
                            Dealership = ""
                            Amount = nullable 0.0
                            }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->
                    schema.SalesInsert(id.CarID, id.CustomerID, id.StaffID, id.Dealership, id.Amount) |> ignore
                    Sorter id.UserName id.Password id.CustomerID.Value "SALE" data
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Forbidden
            
        with ex ->
            HttpStatusCode.InternalServerError
namespace Services

open System
open System.Net
open FSharp.Core
open FSharp.Data
open System.Net.Mail
open Newtonsoft.Json

module emailSender =
    
    type customerData = {
                        CustomerUserName : string
                        CustomerID : Nullable<int>
                        FirstName : string
                        LastName : string
                        Email : string
                        HouseNumber : string
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Status : string
                    }

    type InsertSales = {
                            UserName : string
                            Password : string
                            CarID : Nullable<int>
                            CustomerID : Nullable<int>
                            StaffID : Nullable<int>
                            Dealership : string
                            Amount : Nullable<float>
                            }

    type carData = {
                        CarID : Nullable<int>
                        Make : string
                        Model : string
                        Year : Nullable<int16>
                        Colour : string
                        BodyType : string
                        Doors: Nullable<byte>
                        GearBoxType : string
                        Miles : Nullable<int>
                        EngineSize : Nullable<float>
                        HorsePower : Nullable<int16>
                        SixtyTime : Nullable<float>
                        FuelType : string
                        DriveTrainType : string
                        Co2 : Nullable<int16>
                        Price : Nullable<float>
                        ImageString : string
                        Name : string
                        Number : string
                        Address : string
                        City : string
                        PostCode : string
                    }

    let customerEmail (userName : string)(password: string)(customerID : int) = 
        let json = "{  \"StaffUserName\": \"" + userName + "\",\"StaffPassword\": \"" + password + "\",\"CustomerID\": " + customerID.ToString() + "}"
        
        let json = (Http.RequestString
                        ( "https://teesmo-profilecomponent.azurewebsites.net/CP/CustomerByID",
                        httpMethod = "POST",
                        body = TextRequest json))|> JsonConvert.DeserializeObject<customerData>
        json.Email

    let boughtCar (carID : int) = 
        let json = "{\"carID\" : " + carID.ToString() + "}"
        
        let json = (Http.RequestString
                        ( "https://teesmo-carcomponent.azurewebsites.net/cc/CarDetails",
                        httpMethod = "POST",
                        body = TextRequest json))|> JsonConvert.DeserializeObject<carData []>
        json.[0]
        

    let send (recipient : string)(subject: string)(body : string) =
        let smtp = new SmtpClient(
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        Credentials = new NetworkCredential("teesmo.messages@gmail.com", "TTx5!0PSM69HTz"))
    
        let message = new MailMessage("teesmo.messages@gmail.com", recipient)
        message.Subject <- subject
        message.Body <- body
        smtp.Send(message)

    
    let Sorter (userName : string) (password: string) (customerID : int) (serviceType : string) (data : string) =
        let customerEmail = customerEmail userName password customerID
        
        let mutable subject = ""
        let mutable body = ""

        match serviceType with
        | "SALE" -> 
                    let id = JsonConvert.DeserializeObject<InsertSales>(data)
                    let car = boughtCar(id.CarID.Value)

                    subject <- "YOU BOUGHT A CAR"
                    body <- "Congratulations! You bought a " + car.Year.ToString() + " " + car.Colour + " " + car.Make + " " + car.Model
        | "TRADE" ->
                    subject <- "YOU TRADED YOUR CAR"
                    body <- "Congratulations! You traded in your old car for a new car."
        | "Evaluation" ->
                    subject <- "CAR SERVICE UPDATE"
                    body <- "Your car is undergoing an evaluation."
        | "Service-Light" ->
                    subject <- "CAR SERVICE UPDATE"
                    body <- "Your car is undergoing a light service."
        | "Service-Moderate" ->
                    subject <- "CAR SERVICE UPDATE"
                    body <- "Your car is undergoing a moderate service."
        | "Service-Major" ->
                    subject <- "CAR SERVICE UPDATE"
                    body <- "Your car is undergoing a major service."
        | "MOT" ->
                    subject <- "CAR SERVICE UPDATE"
                    body <- "Your car is being MOT'd."
        | "Service-Complete" ->
                    subject <- "CAR SERVICE COMPLETE"
                    body <- "The service on your car has been complete. You may now come and collect your car."
        |_->
                    subject <- ""
                    body <- ""

        send customerEmail subject body

        0|> ignore
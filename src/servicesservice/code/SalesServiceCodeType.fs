namespace Services

open System
open System.IO
open System.Net
open FSharp.Core
open DataLayer.DLCode
open System.Drawing
open System.Text

module SSTypes =

    type SalesAuth = {
                        UserName : string
                        Password : string
                        }

    type SalesAuthDealership = {
                        UserName : string
                        Password : string
                        DealershipName : string
                        }

    type SalesAuthplusID = {
                            UserName : string
                            Password : string
                            SalesID : Nullable<int>
                            }

    type SalesAuthplusInvoice = {
                            UserName : string
                            Password : string
                            InvoiceID : Nullable<int>
                            }

    type position = {
                    Position : string
                    }
    
    type Sales = {
                    SaleID : Nullable<int>
                    CarDescription : string
                    StaffName : string
                    DealershipName : string
                    CustomerName : string
                    Date : Nullable<DateTime>
                    }

    type SalesView = {
                    SaleID : Nullable<int>
                    Amount : Nullable<float>
                    CarID : Nullable<int>
                    CarDescription : string
                    StaffID : Nullable<int>
                    StaffName : string
                    DealershipName : string
                    CustomerID : Nullable<int>
                    CustomerName : string
                    Date : Nullable<DateTime>
                    InvoiceID : Nullable<int>
                    }
    
    type InsertSales = {
                            UserName : string
                            Password : string
                            CarID : Nullable<int>
                            CustomerID : Nullable<int>
                            StaffID : Nullable<int>
                            Dealership : string
                            Amount : Nullable<float>
                            }
                            
namespace Services

open System
open System.IO
open System.Net
open FSharp.Core
open DataLayer.DLCode
open System.Drawing
open System.Text
open Newtonsoft.Json
open MSTypes
open emailSender

module MSCode =
    
    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()

    let emptyServiceBooking = {
                                ServiceBookingID = nullable 0
                                CustomerID = nullable 0
                                CustomerName = ""
                                CarID = nullable 0
                                Car = ""
                                DateBooked = nullable(new DateTime())
                                DealershipName = ""
                                Completed = nullable false
                            }

    let emptyServiceLog = {
                                ServiceLogID = nullable 0
                                ServiceBookingID = nullable 0
                                StaffID = nullable 0
                                StaffName = ""
                                ServiceProcess = ""
                                Amount = nullable 0.0
                                Time = nullable 0.0
                                ServiceProcessStatus = ""
                                Notes = ""
                                AdditionalCost = nullable 0.0
                            }

    let AuthCheck (creds : ServiceAuth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]
    
    let GetAll(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<ServiceAuth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") with
            | true ->
                        query {
                            for row in schema.ServiceBookingGetbyTodaysDate() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    ServiceBookingID = row.ServiceBookingID
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    CarID = row.CarId
                                                    Car = row.Car
                                                    DateBooked = row.DateBooked
                                                    DealershipName = row.Name
                                                    Completed = row.Completed
                                                }) |> Seq.toArray
            |_-> [|emptyServiceBooking|]
        with ex ->
            [|emptyServiceBooking|]

    let GetAllCompleted(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<ServiceAuth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") with
            | true ->
                        query {
                            for row in schema.ServiceBookingGetbyCompleted() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    ServiceBookingID = row.ServiceBookingID
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    CarID = row.CarId
                                                    Car = row.Car
                                                    DateBooked = row.DateBooked
                                                    DealershipName = row.Name
                                                    Completed = row.Completed
                                                }) |> Seq.toArray
            |_-> [|emptyServiceBooking|]
        with ex ->
            [|emptyServiceBooking|]

    let GetAllbyDealership(auth : string) =
        try
            let id = try
                            JsonConvert.DeserializeObject<ServiceAuthplusDealership>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                                Dealership = ""
                            }

            let creds = {
                            UserName = id.UserName
                            Password = id.Password
                        }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Mechanic") with
            | true ->
                        query {
                            for row in schema.ServiceBookingGetbyDealershipTodaysDate(id.Dealership) do
                            select row
                        } |> Seq.map (fun row -> {
                                                    ServiceBookingID = row.ServiceBookingID
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    CarID = row.CarId
                                                    Car = row.Car
                                                    DateBooked = row.DateBooked
                                                    DealershipName = row.Name
                                                    Completed = row.Completed
                                                }) |> Seq.toArray
            |_-> [|emptyServiceBooking|]
        with ex ->
            [|emptyServiceBooking|]

    let GetAllbyDealershipCompleted(auth : string) =
        try
            let id = try
                            JsonConvert.DeserializeObject<ServiceAuthplusDealership>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                                Dealership = ""
                            }

            let creds = {
                            UserName = id.UserName
                            Password = id.Password
                        }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Mechanic") with
            | true ->
                        query {
                            for row in schema.ServiceBookingGetbyDealershipCompleted(id.Dealership) do
                            select row
                        } |> Seq.map (fun row -> {
                                                    ServiceBookingID = row.ServiceBookingID
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    CarID = row.CarId
                                                    Car = row.Car
                                                    DateBooked = row.DateBooked
                                                    DealershipName = row.Name
                                                    Completed = row.Completed
                                                }) |> Seq.toArray
            |_-> [|emptyServiceBooking|]
        with ex ->
            [|emptyServiceBooking|]
            
    let GetById(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<ServiceBookingID>(data)
                     with ex ->
                        {
                            ServiceBookingID = nullable 0
                        }

            query {
                for row in schema.ServiceBookingGetByID(id.ServiceBookingID) do
                select row
            } |> Seq.map (fun row -> {
                                        ServiceBookingID = row.ServiceBookingID
                                        CustomerID = row.CustomerID
                                        CustomerName = row.CustomerName
                                        CarID = row.CarId
                                        Car = row.Car
                                        DateBooked = row.DateBooked
                                        DealershipName = row.Name
                                        Completed = row.Completed
                                        }) |> Seq.toArray
            
        with ex ->
            [|emptyServiceBooking|]

    let GetCompletedServiceLogsByServiceBookingID(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<ServiceBookingID>(data)
                     with ex ->
                        {
                            ServiceBookingID = nullable 0
                        }

            query {
                for row in schema.ServiceLogsGetByServiceBookingIDCompleted(id.ServiceBookingID) do
                select row
            } |> Seq.map (fun row -> {
                                        ServiceLogID = row.LogID
                                        ServiceBookingID = row.ServiceBookingID
                                        StaffID = row.StaffId
                                        StaffName = row.StaffName
                                        ServiceProcess = row.Process
                                        Amount = row.Amount
                                        Time = row.Time
                                        ServiceProcessStatus = row.ServiceProcessStatus
                                        Notes = row.Notes
                                        AdditionalCost = row.AdditionalCost
                                    }) |> Seq.toArray
        with ex ->
            [|emptyServiceLog|]

    let GetServiceLogsByServiceBookingID(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<ServiceBookingID>(data)
                     with ex ->
                        {
                            ServiceBookingID = nullable 0
                        }

            query {
                for row in schema.ServiceLogsGetByServiceBookingID(id.ServiceBookingID) do
                select row
            } |> Seq.map (fun row -> {
                                        ServiceLogID = row.LogID
                                        ServiceBookingID = row.ServiceBookingID
                                        StaffID = row.StaffId
                                        StaffName = row.StaffName
                                        ServiceProcess = row.Process
                                        Amount = row.Amount
                                        Time = row.Time
                                        ServiceProcessStatus = row.ServiceProcessStatus
                                        Notes = row.Notes
                                        AdditionalCost = row.AdditionalCost
                                    }) |> Seq.toArray
        with ex ->
            [|emptyServiceLog|]

    let GetByServiceLogID(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<ServiceLogID>(data)
                     with ex ->
                        {
                            ServiceLogID = nullable 0
                        }

            query {
                for row in schema.ServiceLogsGetByServiceLogID(id.ServiceLogID) do
                select row
            } |> Seq.map (fun row -> {
                                        ServiceLogID = row.LogID
                                        ServiceBookingID = row.ServiceBookingID
                                        StaffID = row.StaffId
                                        StaffName = row.StaffName
                                        ServiceProcess = row.Process
                                        Amount = row.Amount
                                        Time = row.Time
                                        ServiceProcessStatus = row.ServiceProcessStatus
                                        Notes = row.Notes
                                        AdditionalCost = row.AdditionalCost
                                        }) |> Seq.toArray
            
        with ex ->
            [|emptyServiceLog|]

    let insertServiceLog(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<InsertServiceLog>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            ServiceBookingID = nullable 0
                            StaffID = nullable 0
                            Process = ""
                            Notes = ""
                            AdditionalCost = nullable 0.0
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Mechanic") with
            | true ->
                    schema.ServiceLogInsert(id.ServiceBookingID, id.StaffID, id.Process, id.Notes, id.AdditionalCost) |> ignore
                    let customerID = (GetById ("{\"ServiceBookingID\": " + id.ServiceBookingID.ToString() + "}")).[0].CustomerID.Value
                    Sorter id.UserName id.Password customerID id.Process data
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Forbidden
        with ex ->
            HttpStatusCode.InternalServerError

    let completeServiceLog(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<InsertServiceLogCompleted>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            ServiceLogID = nullable 0
                            Notes = ""
                            AdditionalCost = nullable 0.0
                            }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Mechanic") with
            | true ->
                    schema.ServiceLogComplete(id.ServiceLogID, id.Notes, id.AdditionalCost) |> ignore
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Forbidden
        with ex ->
            HttpStatusCode.InternalServerError

    let insertServiceCompleted(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<InsertServiceCompleted>(data)
                     with ex ->
                        {
                        UserName = ""
                        Password = ""
                        ServiceBookingID = nullable 0
                        DealershipName = ""
                        CustomerID = nullable 0
                        StaffID = nullable 0
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = AuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Mechanic") with
            | true ->
                    schema.ServiceCompletedInsert(id.ServiceBookingID, id.DealershipName, id.CustomerID, id.StaffID) |> ignore
                    Sorter id.UserName id.Password id.CustomerID.Value "Service-Complete" data
                    HttpStatusCode.OK
            |_-> HttpStatusCode.Forbidden
        with ex ->
            HttpStatusCode.InternalServerError
namespace Services

open System
open System.IO
open System.Net
open FSharp.Core
open DataLayer.DLCode
open System.Drawing
open System.Text
open Newtonsoft.Json

module MSTypes =
    
    type ServiceAuth = {
                        UserName : string
                        Password : string
                        }

    type ServiceAuthplusID = {
                            UserName : string
                            Password : string
                            ExpenseID : Nullable<int>
                            }

    type ServiceAuthplusDealership = {
                            UserName : string
                            Password : string
                            Dealership : string
                            }

    type position = {
                    Position : string
                    }

    type ServiceBookingID = {
                            ServiceBookingID : Nullable<int>
                            }

    type ServiceLogID = {
                            ServiceLogID : Nullable<int>
                            }
    
    type serviceBookingData = {
                                ServiceBookingID : Nullable<int>
                                CustomerID : Nullable<int>
                                CustomerName : string
                                CarID : Nullable<int>
                                Car : string
                                DateBooked : Nullable<DateTime>
                                DealershipName : string
                                Completed : Nullable<bool>
                            }
    
    type serviceLogData = {
                                ServiceLogID : Nullable<int>
                                ServiceBookingID : Nullable<int>
                                StaffID : Nullable<int>
                                StaffName : string
                                ServiceProcess : string
                                Amount : Nullable<float>
                                Time : Nullable<float>
                                ServiceProcessStatus : string
                                Notes : string
                                AdditionalCost : Nullable<float>
                            }

    type InsertServiceLog = {
                            UserName : string
                            Password : string
                            ServiceBookingID : Nullable<int>
                            StaffID : Nullable<int>
                            Process : string
                            Notes : string
                            AdditionalCost : Nullable<float>
                            }

    type InsertServiceLogCompleted = {
                            UserName : string
                            Password : string
                            ServiceLogID : Nullable<int>
                            Notes : string
                            AdditionalCost : Nullable<float>
                            }

    type InsertServiceCompleted = {
                                    UserName : string
                                    Password : string
                                    ServiceBookingID : Nullable<int>
                                    DealershipName : string
                                    CustomerID : Nullable<int>
                                    StaffID : Nullable<int>
                                    }
                            